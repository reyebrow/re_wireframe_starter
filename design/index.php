<!DOCTYPE html>
<?php require_once( '../frameworks/PHP/directorylist-design.php' ); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />
	<title><?php print get_name() . " Wireframes"; ?></title>
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="../frameworks/foundation/stylesheets/foundation.css">
	<link rel="stylesheet" href="../frameworks/foundation/stylesheets/app.css">

	<link rel="stylesheet" href="../style.css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="v1/foundation/stylesheets/ie.css">
	<![endif]-->	
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<!-- container -->
  <div class="row">
    <div class="twelve columns">
      <div class="panel callout">
        <h1><?php print ucfirst(get_name()); ?></h1> 
        <p>The following links will take you to design mockups, as a part of the <?php print get_name(); ?> design project</p>        
      </div>
    </div>
  </div>

  <div class="row">
    <div class="twelve columns">
        <ul>
          <?php print versions(getcwd()); ?>
        </ul>
    </div>
  </div>


  <!-- Included JS Files -->
	<!-- Combine and Compress These JS Files -->
  <script src="../frameworks/foundation/javascripts/jquery.js"></script>
	<script src="../frameworks/foundation/javascripts/app.js"></script>
	<script src="../frameworks/foundation/javascripts/foundation.js"></script>
	<!-- End Combine and Compress These JS Files -->	
</body>
</html>