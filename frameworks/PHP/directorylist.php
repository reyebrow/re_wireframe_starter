<?php function template_directory( $path ){

  //Scan the directoru and print out the ones we find. 
  // (nothing special going on here)
  $dirtoscan = ($path);
  $directorylistings = scandir($dirtoscan);
  $dirs = "";
  
  foreach ($directorylistings as $file) {
    $dirinfo = pathinfo($file);
    $stats = stat($file);
    $name = current(explode('.', $file));
    $name = str_replace("_", " ", $name);
    $ext = end(explode('.', $file));

    if (is_file($file) && ( $ext == "php" || $ext == "htm" || $ext == "html" ) && $name !="summary") { ?>

      <li>
        <div class="panel">
          <h3><a class='' href='<?php print $file ?>'><span class="glyph general sample">0</span> <?php print ucwords($name); ?></a></h3>
          <div class="row collapse">
            <div class="columns six subheader">Modified on: <?php print date('m/j/Y h:i a' , $stats['mtime']); ?></div>
            <div  class="columns two"><a class='' href='<?php print $file; ?>'><img width='50' src="../frameworks/PHP/pc.png"></a></div>
            <div  class="columns two"><a class='' href='<?php print $file; ?>#?protoFluid=320%2C480%2CPhone&protoFluidOrientation=true'><img width='50' src="../frameworks/PHP/phone.png"></a></div>
            <div  class="columns two"><a class='' href='<?php print $file; ?>#?protoFluid=768%2C1024%2CTablet&protoFluidOrientation=true'><img width='50' src="../frameworks/PHP/tablet.png"></a></div>
          </div>
        </div>
      </li>
      <?php
    }
  }
  return $dirs;
}

function versions( $path ){

  //Scan the directoru and print out the ones we find. 
  // (nothing special going on here)
  $dirtoscan = $path;
  $directorylistings = scandir($dirtoscan);
  $dirs = "";
  
  $versions = "";

  foreach ($directorylistings as $directory) {
    $dirinfo = pathinfo($directory);
    $stats = stat($directory);
    $name = current(explode('.', $directory));
    $name = str_replace("_", " ", $name);
    $ext = end(explode('.', $directory));

    if (is_dir($directory) && $directory !="frameworks" && $directory !="." && $directory !=".." && $directory !=".git") { ?>

      <div class="panel">
      
        <div class="row">
          <div class="columns six"><h2><a class='' href='<?php print $directory; ?>'><?php print ucwords($name); ?></a></h2></div>
          <?php if ($directory != 'design'): ?>
          <div class="columns three"><a class='radius label right black' href='<?php print $directory . "/summary.php"; ?>'>List of Templates...</a></div>
          <div  class="columns one"><a class='' href='<?php print $directory; ?>'><img width='50' src="frameworks/PHP/pc.png"></a></div>
          <div  class="columns one"><a class='' href='<?php print $directory; ?>/#?protoFluid=320%2C480%2CPhone&protoFluidOrientation=true'><img width='50' src="frameworks/PHP/phone.png"></a></div>
          <div  class="columns one"><a class='' href='<?php print $directory; ?>/#?protoFluid=768%2C1024%2CTablet&protoFluidOrientation=true'><img width='50' src="frameworks/PHP/tablet.png"></a></div>
          <?php endif; ?>
        </div>
        <div class="row">
          <div class="subheader">Modified on: <?php print date('m/j/Y h:i a' , $stats['mtime']); ?></div>
       </div>
      </div>
      <?php
    }
  }
  
  return $dirs;

}

function get_name(){
  $folders = explode("/", getcwd());
  $name = end($folders);
  $name = str_replace("_", " ", $name);
  return $name;
}