<?php function template_directory( $path ){

  //Scan the directoru and print out the ones we find. 
  // (nothing special going on here)
  $dirtoscan = ($path);
  $directorylistings = scandir($dirtoscan);
  $dirs = "";
  
  foreach ($directorylistings as $file) {
    $dirinfo = pathinfo($file);
    $stats = stat($file);
    $name = current(explode('.', $file));
    $name = str_replace("_", " ", $name);
    $ext = end(explode('.', $file));

    if (is_file($file) && ( $ext == "php" || $ext == "htm" || $ext == "html" ) && $name !="summary") { ?>

      <li><div class="panel">
      <a class='' href='<?php print $file ?>'><h2><?php print ucwords($name); ?></h2></a>
      <div class="row">
        <div class="columns six subheader">Modified on: <?php print date('m/j/Y h:i a' , $stats['mtime']); ?></div>
      </div>
      </div></li>
      <?php
    }
  }
  return $dirs;
}

function versions( $path ){

  //Scan the directoru and print out the ones we find. 
  // (nothing special going on here)
  $dirtoscan = $path;
  $directorylistings = scandir($dirtoscan);
  $dirs = "";
  
  $versions = "";

  foreach ($directorylistings as $directory) {
    $dirinfo = pathinfo($directory);
    $stats = stat($directory);
    $name = current(explode('.', $directory));
    $name = str_replace("_", " ", $name);
    $ext = end(explode('.', $directory));

    if (is_dir($directory) && $directory !="frameworks" && $directory !="." && $directory !=".." && $directory !=".git") { ?>

      <div class="panel">
      
        <div class="row">
          <div class="columns six"><h2><a class='' href='<?php print $directory . "/summary.php"; ?>'><?php print ucwords($name); ?></a></h2></div>
          <div class="columns three"><a class='radius label right black' href='<?php print $directory . "/summary.php"; ?>'>List of Mockups...</a></div>
        </div>
        <div class="row">
          <div class="subheader">Modified on: <?php print date('m/j/Y h:i a' , $stats['mtime']); ?></div>
          <?php if (is_file( $directory . "/Notes.txt" ) ) { include_once $directory . "/Notes.txt"; }?>
       </div>
      </div>
      <?php
    }
  }
  
  return $dirs;

}

function get_name(){
  $folders = explode("/", getcwd());
  $name = end($folders);
  $name = str_replace("_", " ", $name);
  return $name;
}