  </div><!-- container -->
	
<!-- .ten.columns.offset-by-one.centered -->

	<footer class="row">
	
    <!-- LEFT -->	
    <div class="columns four">
  		<h4>Tweets</h4>
        	<div class="tweet-authorphoto left"><img src="../frameworks/di/50x50/eee/aaa" /></div>
        	<span class="tweet-author"><a href="http://twitter.com/StateDept">StateDept</a></span>
        	<span class="tweet-text"><p>Acting Spokesperson Mark Toner: We appreciate the ongoing expeditious consideration of this case by Egyptian authorities. #<a href="http://twitter.com/#!/search?q=%23Egypt" title="#Egypt" rel="nofollow">Egypt</a></p></span>
        	<div class="tweet-time"><a href="http://twitter.com/StateDept/status/139761130179526657">2 hours 35 min ago.</a></div>
        	<div class="tweet-divider"></div>    
        </li>
  		</ul>
    </div>
        
        	
    <!-- CENTER -->	
    <div class="columns four">
      <h4>Connect to Us</h4>
      <p>(604)-123-4567</br><a href="mailtio:bobloblaw@blahblahblah.blah">info@ourcompany.com</a></p>
        <a class="fc-webicon facebook" href="#">Facebook</a>
        <a class="fc-webicon googleplus" href="#">GooglePlus</a>
        <a class="fc-webicon linkedin" href="#">LinkedIn</a>
        <a class="fc-webicon mail" href="#">Mail</a>
        <a class="fc-webicon pinterest" href="#">Pinterest</a>
        <a class="fc-webicon rss" href="#">RSS</a>
        <a class="fc-webicon twitter" href="#">Twitter</a>
        <a class="fc-webicon youtube" href="#">YouTube</a>
    </div>
    
    
    
    <!-- RIGHT -->
    <div class="columns four">
      <h4>Newsletter Sign Up</h4>
      <p>You should sign up for our newsletter using the form below.</p>
        <form method="get" class="nice" id="searchform" action="">
        <div class="row collapse">
          <div class="eight mobile-three columns">
            <input type="text" class="" name="s" id="s" placeholder="Enter your email address">
          </div>
          <div class="four mobile-one columns">
            <input type="submit" class="submit postfix button " name="submit" id="searchsubmit" value="Sign Up">
          </div>
        </div>
        </form>
    </div>

	</footer><!--row -->

</div>


<div id="myModal" class="reveal-modal">
     <h2>Awesome. I have it.</h2>
     <p class="lead">Your couch.  I it's mine.</p>
     <p>Im a cool paragraph that lives inside of an even cooler modal. Wins</p>
     <a class="close-reveal-modal">&#215;</a>
</div>


<div id="lightbox" class="reveal-modal">
     <img src="../frameworks/di/800x600/eee/aaa" />
</div>

	<!-- Included JS Files -->
	<!-- Combine and Compress These JS Files -->
	<script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.js"></script>
  <script type='text/javascript' src='../frameworks/protofluid/protoFluid3.02.js'></script>

  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.orbit.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.forms.js"></script>

  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.accordion.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.alerts.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.buttons.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.tabs.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.navigation.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.reveal.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.placeholder.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/jquery.foundation.tooltips.js"></script>


  <script type='text/javascript' src="../frameworks/foundation/javascripts/app.js"></script>
  <script type='text/javascript' src="../frameworks/jquery.menuToSelect.js/scripts/jquery.menuToSelect.min.js"></script>
  <script type='text/javascript' src="script.js"></script>
  <script type='text/javascript' src="../frameworks/foundation/javascripts/modernizr.foundation.js"></script>

	<!-- End Combine and Compress These JS Files -->
	

</body>
</html>