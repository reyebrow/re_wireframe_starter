a<?php include_once('includes/header.php');?>

<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a></li>
    <li>/</li>
    <li><a href="media.php">Media Room</a> </li>
    <li>/</li>
    <li class="">Videos</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

  <h1>Videos</h1>
  
	<div class="row">
    <div class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">A Big day out</a></h4>
		</div>
		<div  class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">10K Charity Run</a></h4>
		</div>
		<div  class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">10K Charity Run</a></h4>
		</div>
  </div>
  <div class="row">
		<div class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">First council meeting</a></h4>
		</div>
		<div class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">Interview with Mayor</a></h4>
		</div>
		<div class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">Interview with Mayor</a></h4>
		</div>
  </div>  
	<div class="row">
    <div class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">A Big day out</a></h4>
		</div>
		<div  class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">10K Charity Run</a></h4>
		</div>
		<div  class="four columns">
      <a href="video-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/video">
			<h4><a href="video-item.php">10K Charity Run</a></h4>
		</div>
  </div>

  <!-- PAGINATION -->
  <ul class="pagination">
    <li class="unavailable"><a href="">&laquo;</a></li>
    <li class="current"><a href="">1</a></li>
    <li><a href="">2</a></li>
    <li><a href="">3</a></li>
    <li><a href="">4</a></li>
    <li class="unavailable"><a href="">&hellip;</a></li>
    <li><a href="">12</a></li>
    <li><a href="">13</a></li>
    <li><a href="">&raquo;</a></li>
  </ul>

 </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h4>Media Room</h4>
      <ul>
        <li><a href="news.php">News</a></li>
        <li><a href="galleries.php">Photo Galleries</a></li>
        <li><a href="videos.php">Videos</a></li>
     </ul>
    </div>

    <div class="panel">
      <h4>Popular Content</h4>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>
  </div>

</div><!-- ROW-->


<?php include_once('includes/footer.php');?>