<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="events.php">Events</a> </li>
    <li>/</li>
    <li class="active">Past Events</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Past Events for 2011</h1>	
			<div>
        <h4><a href="events-item.php">A collection of minds</a></h4> 
        <div>November 2, 2011 - November 3, 2011</div>  
			</div>
			<div>
        <h4><a href="events-item.php">A Celebration of Achievements</a></h4> 
        <div>September 16, 2011 - September 18, 2011</div>  
			</div>
			<div>
        <h4><a href="events-item.php">Transformation and Innovation</a></h4> 
        <div>September 7, 2011</div>  
			</div>
			<div>
        <h4><a href="events-item.php">A Celebration of Achievements</a></h4> 
        <div>June 16, 2011</div>  
			</div>
			<div>
        <h4><a href="events-item.php">A collection of minds</a></h4> 
        <div>November 2, 2011 - November 3, 2011</div>  
			</div>
			<div>
        <h4><a href="events-item.php">A Celebration of Achievements</a></h4> 
        <div>September 16, 2011 - September 18, 2011</div>  
			</div>
			<div>
        <h4><a href="events-item.php">Transformation and Innovation</a></h4> 
        <div>September 7, 2011</div>  
			</div>
			<div>
        <h4><a href="events-item.php">A Celebration of Achievements</a></h4> 
        <div>June 16, 2011</div>  
			</div> 
  </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>Events</h2>
      <ul>
        <li><a href="events-calendar.php">Full Calendar</a></li>
        <li><a href="events-past.php">Past Events</a></li>
      </ul>
    </div>

    <div class="panel">
      <h2>Archive by date</h2>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>