<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="media.php">Media Room</a> </li>
    <li>/</li>
    <li class="active">Press Releases</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Press Releases</h1>				
      <div>
        <h3><a href="news-item.php">Why Unions Matter</a></h3> 
        <div>Novemver 21, 2011</div>  
        <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.</p>
      </div>
      <div>
        <h3><a href="news-item.php">IBT Election: Hoffa-Hall Slate Receives 60 Percent Of Vote</a></h3>
        <div>October 21, 2011</div>
        <p>General President Jim Hoffa, his running mate for General Secretary-Treasurer Ken Hall and their entire slate, were elected by a wide margin in the 2011 Election of International Union Officers.</p>
      </div>
      <div>
        <h3><a href="news-item.php">More interesting Stuff</a></h3> 
        <div>September 1, 2011</div>  
        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
      </div>
      <div>
        <h3><a href="news-item.php">Some other long title</a></h3> 
        <div>June 11, 2011</div>  
        <p>I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty.</p>
      </div>
      <div>
        <h3><a href="news-item.php">And yet another thing that is making the news today</a></h3> 
        <div>April 21, 2011</div>  
        <p>General President Jim Hoffa, his running mate for General Secretary-Treasurer Ken Hall and their entire slate, were elected by a wide margin in the 2011 Election of International Union Officers.</p>
      </div>
    <!-- PAGINATION -->
    <ul class="pagination">
      <li class="unavailable"><a href="">&laquo;</a></li>
      <li class="current"><a href="">1</a></li>
      <li><a href="">2</a></li>
      <li><a href="">3</a></li>
      <li><a href="">4</a></li>
      <li class="unavailable"><a href="">&hellip;</a></li>
      <li><a href="">12</a></li>
      <li><a href="">13</a></li>
      <li><a href="">&raquo;</a></li>
    </ul>

  </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h3>Media Room</h3>
      <ul>
        <li><a href="news.php">Press Releases</a></li>
        <li><a href="galleries.php">Photo Galleries</a></li>
        <li><a href="videos.php">Videos</a></li>
      </ul>
    </div>

    <div class="panel">
      <h2>Archive by date</h2>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>

</div><!-- ROW-->


<?php include_once('includes/footer.php');?>