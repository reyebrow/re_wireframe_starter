<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="media.php">Media</a> </li>
    <li>/</li>
    <li><a href="videos.php">Videos</a> </li>
    <li>/</li>
    <li class="active">Interview with the Mayor</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  


  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Interview with the Mayor</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec mauris pulvinar erat faucibus euismod. Donec rutrum euismod libero, vel hendrerit arcu rhoncus sit amet. Fusce vel augue ac libero luctus ornare. Phasellus accumsan dapibus tincidunt.</p>
    <!-- ORBIT SLIDESHOW -->
    <div class="orbit featured">
      <img src="../frameworks/di/980x4:3/ccc/969696/video" alt="Overflow: Hidden No More" />
    </div>
     <div class="row">
      <div class="eight columns offset-by-four">
      <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee13bd71905212d"></script>
        <!-- AddThis Button END -->
      </div>
    </div>
  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h3>Media Room</h3>
      <ul>
        <li><a href="news.php">News</a></li>
        <li><a href="galleries.php">Photo Galleries</a></li>
        <li><a href="videos.php">Videos</a></li>
     </ul>
    </div>

    <div class="panel">
      <h4>Popular Content</h4>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>