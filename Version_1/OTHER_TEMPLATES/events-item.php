<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="events.php">Events</a> </li>
    <li>/</li>
    <li class="active">A collection of minds</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 
    <h1>A collection of minds</h1>
    <div class="subheader">November 21, 2011 - November 3, 2011<br />1:00pm - 1:30pm<br />Viscount Gort Hotel, Winnipeg, MB<br />
    <div class="ctools-collapsible-container ctools-collapsed ctools-collapsible-processed"><span class="ctools-toggle"></span><div class="ctools-collapsible-handle">&raquo; <a href="#">Map (click to open)</a></div><div class="ctools-collapsible-content" style="display: block; ">  			<div class="field-item even">
    <div style="width: auto; height: 450px;" id="openlayers-container-openlayers-map-c5326980" class="openlayers-container openlayers-container-map-event_location_map">
      <div style="width: auto; height: 450px; " id="openlayers-map-c5326980" class="openlayers-map openlayers-map-event_location_map openlayers-processed olMap openlayers-openlayers_behavior_attribution-processed openlayers-openlayers_behavior_keyboarddefaults-processed openlayers-openlayers_behavior_navigation-processed openlayers-openlayers_behavior_popup-processed openlayers-openlayers_behavior_zoompanel-processed openlayers-openlayers_behavior_zoomtolayer-processed"><div id="OpenLayers.Map_2_OpenLayers_ViewPort" style="position: relative; overflow-x: hidden; overflow-y: hidden; width: 100%; height: 100%; " class="olMapViewport olControlAttributionActive olControlKeyboardDefaultsActive olControlDragPanActive olControlZoomBoxActive olControlPinchZoomActive olControlNavigationActive olControlSelectFeatureActive olControlZoomPanelActive"><div id="OpenLayers.Map_2_events" style="position: absolute; width: 100%; height: 100%; z-index: 999; "><div id="OpenLayers.Map_2_OpenLayers_Container" style="position: absolute; z-index: 749; left: 0px; top: 0px; "><div id="OpenLayers.Layer.Vector_15" style="position: absolute; width: 100%; height: 100%; z-index: 325; left: 0px; top: 0px; " dir="ltr" class="olLayerDiv"><svg id="OpenLayers.Layer.Vector_15_svgRoot" width="582" height="450" viewBox="0 0 582 450"></svg></div><div id="OpenLayers.Layer.XYZ_28" style="position: absolute; width: 100%; height: 100%; z-index: 105; " dir="ltr" class="olLayerDiv"><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 354px; top: 158px; width: 256px; height: 256px; "><img id="OpenLayersDiv42" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5180/11214.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 98px; top: 158px; width: 256px; height: 256px; "><img id="OpenLayersDiv44" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5179/11214.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: -158px; top: 158px; width: 256px; height: 256px; "><img id="OpenLayersDiv46" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5178/11214.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: -158px; top: 414px; width: 256px; height: 256px; "><img id="OpenLayersDiv48" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5178/11215.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 98px; top: 414px; width: 256px; height: 256px; "><img id="OpenLayersDiv50" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5179/11215.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 354px; top: 414px; width: 256px; height: 256px; "><img id="OpenLayersDiv52" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5180/11215.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 610px; top: 414px; width: 256px; height: 256px; "><img id="OpenLayersDiv54" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5181/11215.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 610px; top: 158px; width: 256px; height: 256px; "><img id="OpenLayersDiv56" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5181/11214.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 610px; top: -98px; width: 256px; height: 256px; "><img id="OpenLayersDiv58" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5181/11213.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 354px; top: -98px; width: 256px; height: 256px; "><img id="OpenLayersDiv60" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5180/11213.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: 98px; top: -98px; width: 256px; height: 256px; "><img id="OpenLayersDiv62" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5179/11213.png"></div><div style="overflow-x: hidden; overflow-y: hidden; position: absolute; z-index: 1; left: -158px; top: -98px; width: 256px; height: 256px; "><img id="OpenLayersDiv64" style="position: relative; width: 256px; height: 256px; " class="olTileImage" src="http://tile.openstreetmap.org/15/5178/11213.png"></div></div><div id="OpenLayers.Layer.Vector.RootContainer_81" style="position: absolute; width: 100%; height: 100%; z-index: 725; left: 0px; top: 0px; " dir="ltr" class="olLayerDiv"><svg id="OpenLayers.Layer.Vector.RootContainer_81_svgRoot" width="582" height="450" viewBox="0 0 582 450"><g id="OpenLayers.Layer.Vector.RootContainer_81_root" style="visibility: visible; " transform=""><g id="OpenLayers.Layer.Vector.RootContainer_81_vroot"></g><g id="OpenLayers.Layer.Vector.RootContainer_81_troot"></g></g><g id="OpenLayers.Layer.Vector_15_root" style="visibility: visible; " transform=""><g id="OpenLayers.Layer.Vector_15_vroot"><image id="OpenLayers.Geometry.Point_17" cx="291.0000000540167" cy="224.99999999790452" r="1" preserveAspectRatio="none" x="279" y="184" width="25" height="41" href="http://f7.dev.internal/sites/all/modules/openlayers/themes/default_dark/markers/marker-black.png" style="opacity: 1" fill="#000000" fill-opacity="1" stroke="#000000" stroke-opacity="1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></image></g><g id="OpenLayers.Layer.Vector_15_troot"></g></g></svg></div></div></div><div id="OpenLayers.Control.Attribution_66" style="position: absolute; z-index: 1001; " class="olControlAttribution olControlNoSelect" unselectable="on">�<a href="http://creativecommons.org/licenses/by-sa/2.0/">CCBYSA</a> 2010
      <a href="http://www.openstreetmap.org/">OpenStreetMap.org</a> contributors</div><div id="OpenLayers.Control.SelectFeature_80" style="position: absolute; z-index: 1004; " class="olControlSelectFeature olControlNoSelect" unselectable="on"></div><div id="OpenLayers.Control.ZoomIn_89" style="position: absolute; z-index: 1006; " class="olControlZoomIn olControlNoSelect" unselectable="on"></div><div id="OpenLayers.Control.ZoomToMaxExtent_90" style="position: absolute; z-index: 1007; " class="olControlZoomToMaxExtent olControlNoSelect" unselectable="on"></div><div id="OpenLayers.Control.ZoomOut_91" style="position: absolute; z-index: 1008; " class="olControlZoomOut olControlNoSelect" unselectable="on"></div><div id="OpenLayers.Control.ZoomPanel_88" style="position: absolute; z-index: 1008; " class="olControlZoomPanel olControlNoSelect" unselectable="on"><div class="olControlZoomInItemInactive"></div><div class="olControlZoomToMaxExtentItemInactive"></div><div class="olControlZoomOutItemInactive"></div></div></div></div>
    </div>		</div>
	</div></div> 

  </div>
    <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus. Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus.</p>
    <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus. Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus.</p>
    <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus. Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus.</p>

     <div class="row">
      <div class="eight columns offset-by-four">
      <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee13bd71905212d"></script>
        <!-- AddThis Button END -->
      </div>
    </div>

  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>Events</h2>
      <ul>
        <li><a href="events-calendar.php">Full Calendar</a></li>
        <li><a href="events-past.php">Past Events</a></li>
      </ul>
    </div>

    <div class="panel">
      <h2>Archive by date</h2>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>