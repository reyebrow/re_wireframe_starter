<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="resources.php">Resources</a> </li>
    <li>/</li>
    <li><a href="documents.php">Documents</a> </li>
    <li>/</li>
    <li class="active">AGM 2011 Minutes</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 
    <h1>AGM 2011 Minutes</h1>
    <div class="subheader">Created September 19, 2011<br />Last Updated December 2, 2011</div>
    <div class="row">
      <div class="columns twelve">
        <div class="nice large radius white button">
          <a href="/sites/default/files/uploads/publications/Nova%20Scotia%20Office/2011/11/NS%20Child%20Poverty%20ReprtCard2011.pdf" title="Nova Scotia Child Poverty Report Card 2011 ">Download Now</a>
          <br />696 KB PDF
        </div>  
      </div>
    </div><!-- /row -->
    <br />
    <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus. Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus.</p>
    <div class="row">
      <div class="columns twelve">
        <h3>Filed in:</h3>
        <a href="/search/apachesolr_search?filters=tid%3A6177" rel="dcat:keyword">collection-of-waste</a>, <a href="/search/apachesolr_search?filters=tid%3A6852" rel="dcat:keyword">collection-schemes-waste</a>
      </div>
    </div>
    <p>&nbsp;</p>
    <div class="row">
      <div class="eight columns offset-by-four">
      <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee13bd71905212d"></script>
        <!-- AddThis Button END -->
      </div>
    </div>
    
  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>Resources</h2>
      <ul>
        <li><a href="datasets.php">Datasets</a></li>
        <li><a href="documents.php">Documents</a>
          <ul><li><a href="">Sub sub page</a></li></ul>
        </li>
      </ul>
    </div>

    <div class="panel">
      <h3>Popular Content</h3>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>