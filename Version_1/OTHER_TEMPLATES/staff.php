<?php include_once('includes/header.php');?>

<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="about.php">About</a> </li>
    <li>/</li>
    <li class="active">Leadership</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Leadership</h1>			
		  <div class="row highlight collapse">			
        <div class="three columns">
          <img src="../frameworks/di/125x3:4/ccc/969696/bust">
        </div>
        <div class="nine columns ">
          <h2><a href="emira.php">Emira Mears</a></h2>
          <div class="subheader">Chief of Everything</div>
				  <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat...</p>
				</div>
      </div>
			<div class="row highlight collapse">			
        <div class="three columns">
          <img src="../frameworks/di/125x3:4/ccc/969696/bust">
        </div>
        <div class="nine columns ">
          <h2><a href="emira.php">Lauren Bacon</a></h2>
          <div class="subheader">Chief Designer</div>
				  <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat...</p>
				</div>
      </div>
			<div class="row collapse">			
        <div class="three columns">
          <img src="../frameworks/di/125x3:4/ccc/969696/bust">
        </div>
        <div class="nine columns ">
          <h2><a href="emira">Colin Calnan</a></h2>
          <div class="subheader">Chief Wireframe Builder</div>
				  <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat...</p>
				</div>
      </div>
			<div class="row collapse">			
        <div class="three columns">
          <img src="../frameworks/di/125x3:4/ccc/969696/bust">
        </div>
        <div class="nine columns ">
          <h2><a href="emira.php">Christopher Torgalson</a></h2>
          <div class="subheader">Chief Regex Constructor</div>
				  <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat...</p>
				</div>
      </div>
			<div class="row collapse">			
        <div class="three columns">
          <img src="../frameworks/di/125x3:4/ccc/969696/bust">
        </div>
        <div class="nine columns ">
          <h2><a href="emira">Matt Reimer</a></h2>
          <div class="subheader">Chief Git (Mr.)</div>
				  <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat...</p>
				</div>
      </div>
  
    <ul class="pagination">
      <li class="unavailable"><a href="">&laquo;</a></li>
      <li class="current"><a href="">1</a></li>
      <li><a href="">2</a></li>
      <li><a href="">3</a></li>
      <li><a href="">4</a></li>
      <li class="unavailable"><a href="">&hellip;</a></li>
      <li><a href="">12</a></li>
      <li><a href="">13</a></li>
      <li><a href="">&raquo;</a></li>
    </ul>
  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>About</h2>
      <ul>
        <li><a href="leadership.php">Leadership</a></li>
        <li><a href="staff.php">Staff Directory</a></li>
      </ul>
    </div>

    <div class="panel">
      <h3>Popular Content</h3>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>
  </div>
 
</div><!-- ROW-->


<?php include_once('includes/footer.php');?>