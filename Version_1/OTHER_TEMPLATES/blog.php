<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li class="active">Blog</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Blog</h1>

      <h2><a href="blog-item.php">Unlimited leaf collection</a></h2> 
      <div class="subheader"><a href="">Colin Calnan</a><br />November 21, 2011 <span class="divider">|</span> <a href="">2 comments</a></div>  
      <p>It is now fall and the City will be collecting unlimited quantities of leaves from your home (every second week) from October 1 to January 31. 
      <p>Fill your yard trimmings cart (green lid) with your leaves and set it out on your scheduled yard trimmings collection day. Remember, no plastic bags or liners. Use a standard store-bought garbage can or a biodegradable paper bag for your extra leaves. Please do not rake or blow leaves onto the street.</p>
      <p>You can also compost or mulch your leaves as an alternative to setting them out to be collected.</p>
      <a href="">Read more...</a>

			<h2><a href="#">Are you ready for snow?</a></h2>
			<div class="subheader"><a href="">Nik Garkusha</a><br />October 21, 2011 <span class="divider">|</span> <a href="">2 comments</a></div>
      <p>It's getting cold out there and the possibility of snow is imminent. Be ready before the snow falls: be alert for snow in the weather forecast and purchase your snow shovel, salt and snow tires before the snow hits the ground.</p>
      <p>Remember, all property owners and occupants (tenants) are responsible for clearing snow and ice from the full width of sidewalks that surround their property by 10 am on the morning following a snowfall, seven days a week.</p>
      <a href="">Read more...</a>

      <h2 class=""><a href="">Keeping your yard trimmings cart clean</a></h2> 
      <div class="subheader"><a href="">Mark Artega</a><br />September 1, 2011 <span class="divider">|</span> <a href="">2 comments</a></div>  
      <p>With colder weather approaching, sometimes food can freeze to the sides of your cart making it difficult for collection crews to empty. During the holiday season, keep your yard trimmings cart clean by putting a layer of brown paper or newspaper at the bottom of your cart. Wrap any food scraps in newspaper or brown paper bags.</p>
      <a href="">Read more...</a>

    <!-- PAGINATION -->
    <ul class="pagination">
      <li class="unavailable"><a href="">&laquo;</a></li>
      <li class="current"><a href="">1</a></li>
      <li><a href="">2</a></li>
      <li><a href="">3</a></li>
      <li><a href="">4</a></li>
      <li class="unavailable"><a href="">&hellip;</a></li>
      <li><a href="">12</a></li>
      <li><a href="">13</a></li>
      <li><a href="">&raquo;</a></li>
    </ul>

  </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>Blog Categories</h2>
      <ul>
        <li><a href="">Waste Diposal</a></li>
        <li><a href="">Snow clearing</a></li>
        <li><a href="">Garden Trimmings</a></li>
        <li><a href="">Property Taxes</a></li>
      </ul>
    </div>
  </div>
  
  <div id="content" class="columns four"> 
    <div class="panel">
      <h2>Archive by date</h2>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>

</div><!-- ROW-->


<?php include_once('includes/footer.php');?>