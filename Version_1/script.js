$(document).ready(function() {

	/* Use this js doc for all application specific JS */

  /* Select style mobile nav --------------- */
  $.fn.menuToSelect(
    $('ul.menu-to-select'),
    {
      form: '<form class="show-on-phones"><div></div></form>',
      hideOriginal: false,
      selectLabel: '<label>Navigation: </label>'
    }
  ).insertBefore('.breadcrumb');


	/* ORBIT --------------------------------- */
	/* Remove if you don't need :) */
     $(window).load(function() {
         $('.featured').orbit();
     });

     $(window).load(function() {
         $('.gallery').orbit({
          bullets : true,		
          bulletThumbs: true,
          bulletThumbLocation: 'images/',
          timer: 'false',
         });
     });


	/* MODALS --------------------------------- */
	/* Remove if you don't need :) */
  $('.buttonForModal').click(function() {
       $('#myModal').reveal({
         animation: 'fade', //fade, fadeAndPop, none
         animationspeed: 200, //how fast animtions are
         closeOnBackgroundClick: true, //if you click background will modal close?
         dismissModalClass: 'close-reveal-modal' //the class of a button or element that will close an open modal
         });

  });



	/* TABS --------------------------------- */
	/* Remove if you don't need :) */
	
	var tabs = $('dl.tabs');
		tabsContent = $('ul.tabs-content')
	
	tabs.each(function(i) {
		//Get all tabs
		var tab = $(this).children('dd').children('a');
		tab.click(function(e) {
			
			//Get Location of tab's content
			var contentLocation = $(this).attr("href")
			contentLocation = contentLocation + "Tab";
			
			//Let go if not a hashed one
			if(contentLocation.charAt(0)=="#") {
			
				e.preventDefault();
			
				//Make Tab Active
				tab.removeClass('active');
				$(this).addClass('active');
				
				//Show Tab Content
				$(contentLocation).parent('.tabs-content').children('li').css({"display":"none"});
				$(contentLocation).css({"display":"block"});
				
			} 
		});
	});
	
	
	/* PLACEHOLDER FOR FORMS ------------- */
	/* Remove this and jquery.placeholder.min.js if you don't need :) */
	
	$('input, textarea').placeholder();
	
	
	/* DISABLED BUTTONS ------------- */
	/* Gives elements with a class of 'disabled' a return: false; */
	
    var lockNavBar = false;
  /* Windows Phone, sadly, does not register touch events :( */
  if (Modernizr.touch || navigator.userAgent.match(/Windows Phone/i)) {
    $('.nav-bar a.flyout-toggle').on('click.fndtn touchstart.fndtn', function(e) {
      e.preventDefault();
      var flyout = $(this).siblings('.flyout').first();
      if (lockNavBar === false) {
        $('.nav-bar .flyout').not(flyout).slideUp(500);
        flyout.slideToggle(500, function(){
          lockNavBar = false;
        });
      }
      lockNavBar = true;
    });
    $('.nav-bar li.has-flyout').addClass('is-touch');
  } else {
    $('.nav-bar li.has-flyout').hover(function() {
      $(this).children('.flyout').show();
    }, function() {
      $(this).children('.flyout').hide();
    });
  }

  var targetWidth;
  var windowWidth = $(window).width();
  var cookieName = 'COGITO_DEVICE_WIDTH';
  var cookieLifetime = 1; // Cookie expires after 1 day...
  var $typeLink = $('.change-site-link');
  
  // First off, we only care about this link on Mobiles so check the screen width
  if(windowWidth < 768) {
    // We check the Cookie to see if they want to see the mobile or full site
    if(site_type = readCookie(cookieName)) {
      // If the cookie was set to show a mobile site, show a link to the Full site
      if(site_type.toLowerCase().search('mobile') != -1) {
        targetWidth = 1000; 
        $typeLink.html('Mobile site');
      } else {
        targetWidth = 'device-width';
        $typeLink.html('Full site');
      }
    } else {
      $typeLink.html('Full site');
    }
  
    // Handle link clicking...
    $typeLink
      .bind('click', function(){  
        // If they want to switch to full site...         
        if($(this).html().toLowerCase().search('full') != -1) { 
          // Set the device width to wide...
          targetWidth = 1000; 
          // Change the link text...
          $typeLink.html('Mobile site');
          /// Change the class so that it shows up...
        } else {
          targetWidth = 'device-width';
          $typeLink.html('Full site');
        }
        // Set the viewport width based on the above values
        $('meta[name="viewport"]').attr('content', 'initial-scale=1.6, maximum-scale=1.0, width=' + targetWidth);         
        eraseCookie(cookieName); // Erase the existing cookie...
        createCookie(cookieName, $(this).html(), cookieLifetime); // Create a new one...
      });
      
    // Set the viewport width based on the cookie...
    $('meta[name="viewport"]').attr('content', 'initial-scale=1.6, maximum-scale=1.0, width=' + targetWidth);
  }


  /**
   * This function creates a cookie with the specified name, value and expiry date.
   *
   * @param string name The name of the cookie to be created
   * @param string value The value for the cookie
   * @param int days The lifetime, in days, of the cookie
   * @see http://www.quirksmode.org/js/cookies.html
   * @author Peter-Paul Koch
   */
  function createCookie(name, value, days) {
  	if(days) {
  		var date = new Date();
  		date.setTime(date.getTime()+(days*24*60*60*1000));
  		var expires = '; expires='+ date.toGMTString();
  	}
  	else var expires = '';
  	document.cookie = name +'='+ value + expires +'; path=/';
  } /* function createCookie() */
  
  
  /**
   * This function reads the value of a specified cookie.
   *
   * @param string name The name of the cookie to be read
   * @return string|null The value of the cookie or null
   * @see http://www.quirksmode.org/js/cookies.html
   * @author Peter-Paul Koch
   */
  function readCookie(name) {
  	var nameEQ = name + '=';
  	var ca = document.cookie.split(';');
  	for(var i=0;i < ca.length;i++) {
  		var c = ca[i];
  		while(c.charAt(0)==' ') {
  			c = c.substring(1,c.length);
  		}
  		if(c.indexOf(nameEQ) == 0) {
  			return c.substring(nameEQ.length,c.length);
  		}
  	}
  	return null;
  } /* function readCookie() */
  
  
  /**
   * This function destroys a cookie by creating it with an expiry date one day in the past.
   *
   * @param string name The name of the cookie to be destroyed
   * @see function createCookie();
   * @see http://www.quirksmode.org/js/cookies.html
   * @author Peter-Paul Koch
   */
  function eraseCookie(name) {
  	createCookie(name,'',-1);
  } /* function createCookie */

});